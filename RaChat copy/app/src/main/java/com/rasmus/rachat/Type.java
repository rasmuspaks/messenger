package com.rasmus.rachat;

/**
 * Created by arvuti on 18/03/2017.
 */

public enum Type {
    SENT, RECEIVED
}
