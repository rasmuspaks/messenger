package com.rasmus.rachat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

/**
 * Created by arvuti on 18/03/2017.
 */

public class Chat extends Activity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorageRef;
    final String TAG = "MAIN";
    FirebaseDatabase database;
    DatabaseReference myRef;
    ChatView chatView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        chatView = (ChatView) findViewById(R.id.chat_view);
        final String value = getIntent().getExtras().getString("title");
        chatView.getInputEditText().setHint("Write to topic: " + value);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged:signed:inwq");
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference ref1 = database1.getReference("Messages").child(value).child("messages");
        long current = System.currentTimeMillis();
        long cutoff1 = new Date().getTime() - (current - 10000);
        long cutoff = current - 100000000;
        Query oldItems = ref1.orderByChild("timestamp").endAt(cutoff);
        oldItems.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot: snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("Messages").child(value).child("messages");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                Message message = dataSnapshot.getValue(Message.class);
                final ChatMessage chatMessage;
                if (message.type.equals(mAuth.getCurrentUser().getUid())){
                    chatMessage = new ChatMessage(message.message, (long)message.timestamp, ChatMessage.Type.SENT);

                } else {
                    chatMessage = new ChatMessage(message.message, (long)message.timestamp, ChatMessage.Type.RECEIVED);

                }
                chatView.addMessage(chatMessage);


            }



            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {}

        });





        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener(){
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {
               DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Messages").child(value).child("messages");
                Message message = new Message(mAuth.getCurrentUser().getUid(), chatMessage.getMessage(), ServerValue.TIMESTAMP);
                reference.push().setValue(message);
                chatView.getInputEditText().setText("");
                FirebaseDatabase database1 = FirebaseDatabase.getInstance();
                DatabaseReference ref1 = database1.getReference("Messages").child(value).child("messages");
                long current = System.currentTimeMillis();
                long cutoff = new Date().getTime() - (current - 10000000);
                Query oldItems = ref1.orderByChild("timestamp").endAt(cutoff);
                oldItems.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot snap : dataSnapshot.getChildren()){
                            snap.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                return false;
            }
        });
        DatabaseReference lastMessageAdd = FirebaseDatabase.getInstance().getReference("Messages").child(value).child("messages");
        lastMessageAdd.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                DatabaseReference changeTimeStamp = FirebaseDatabase.getInstance().getReference("Chats");
                changeTimeStamp.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                            Topic topic = dataSnapshot1.getValue(Topic.class);
                            if(topic.title.equals(value)){
                                dataSnapshot1.getRef().child("lastmessage").setValue(ServerValue.TIMESTAMP);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    protected void onPause() {
        chatView.removeAllViews();
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }



    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, Topics.class));
    }
}
