package com.rasmus.rachat;

/**
 * Created by arvuti on 18/03/2017.
 */

public class Message {

   public String type;
    public String message;
    public Object timestamp;

    public Message(){

    }
    public Message(String type, String message, Object timestamp) {
        this.type = type;
        this.message = message;
        this.timestamp = timestamp;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }
}
