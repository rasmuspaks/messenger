package com.rasmus.rachat;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import co.intentservice.chatui.models.ChatMessage;

/**
 * Created by arvuti on 18/03/2017.
 */

public class Topic {


    public String title;
    public String image;
    public boolean featured;
    public long lastmessage;


    public Topic(){

    }
    public Topic(String title, String image, boolean featured, long lastMessage) {
        this.title = title;
        this.image = image;
        this.featured = featured;
        this.lastmessage = lastMessage;
    }




}
