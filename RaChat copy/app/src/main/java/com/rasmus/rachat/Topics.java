package com.rasmus.rachat;

import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static com.rasmus.rachat.R.id.action_all_chats;
import static com.rasmus.rachat.R.id.thing_proto;
import static com.rasmus.rachat.R.id.time;
import static com.rasmus.rachat.R.id.view_offset_helper;

import android.app.SearchManager;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

/**
 * Created by arvuti on 18/03/2017.
 */

public class Topics extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorageRef;
    final String TAG = "MAIN";
    FirebaseDatabase database;
    DatabaseReference myRef;
    private TableLayout tableLayout;
    private View.OnClickListener onClickListener;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topics);
        tableLayout = (TableLayout) findViewById(R.id.tableForTopics);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);
        onClickListener = new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Chat.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("title", v.getTag().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        };


        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchTopics(query);
        }









        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
          if (item.getItemId() == R.id.action_all_chats) {
              Intent intent = new Intent(getApplicationContext(), Topics.class);
              startActivity(intent);
          } else if(item.getItemId() == R.id.action_profile){
              Intent intent = new Intent(getApplicationContext(), UserProfile.class);
              startActivity(intent);
          }else {
              Intent intent = new Intent(getApplicationContext(), AddChat.class);
              startActivity(intent);
          }
               return true;
               }


               });


        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                   Log.d(TAG, "OLEN SISSE LOGITUD");
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        database = FirebaseDatabase.getInstance();
        Query query =  database.getReference("Chats")
                .orderByChild("featured");

        query.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, dataSnapshot.toString());
                Topic topic = dataSnapshot.getValue(Topic.class);
                TableRow row = new TableRow(getApplicationContext());
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                TextView textView = new TextView(getApplicationContext());
                if(topic.featured == false) {
                    textView.setText(topic.title + "\n" + "Featured");
                } else {
                    textView.setText(topic.title +  "\n" + "Not featured");
                }
                TextView timestamp1 = new TextView(getApplicationContext());
                if (topic.lastmessage != 0) {
                    long oneDayInMillis = TimeUnit.DAYS.toMillis(1);

                    long timeDifference = System.currentTimeMillis() - topic.lastmessage;

                    if(timeDifference < oneDayInMillis) {
                        timestamp1.setText("Last activity: "+  "\n" + DateFormat.format("hh:mm a", topic.lastmessage).toString());
                    } else {

                        timestamp1.setText("Last activity: "+ "\n" +DateFormat.format("dd MMM - hh:mm a", topic.lastmessage).toString());
                    }
                } else {
                    timestamp1.setText("No messages");
                }
                ImageView imageView = new ImageView(getApplicationContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setLayoutParams(new TableRow.LayoutParams(300, 300));
                imageView.setPadding(5,5,5, 75);
                textView.setPadding(70, 35, 35, 35);
                textView.setTextSize(20);
                new DownLoadImageTask(imageView).execute(topic.image);
                row.setOnClickListener(onClickListener);
                row.setTag(topic.title);
                timestamp1.setPadding(70,10,10,5);
                row.addView(imageView);
                row.addView(textView);
                row.addView(timestamp1);
                row.setPadding(0,0,0, 80);
                tableLayout.addView(row);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                /*Topic topic = dataSnapshot.getValue(Topic.class);
                int childParts = tableLayout.getChildCount();
                if (tableLayout != null) {
                    for (int i = 0; i < childParts; i++) {
                        View viewChild = tableLayout.getChildAt(i);
                        if (viewChild instanceof TableRow) {
                            int rowChildParts = ((TableRow) viewChild).getChildCount();
                            for (int j = 0; j < rowChildParts; j++) {
                                View viewChild2 = ((TableRow) viewChild).getChildAt(j);
                                TextView head = (TextView)viewChild2.findViewWithTag("heading");
                                TextView time = (TextView)viewChild2.findViewWithTag("timestamp");

                                if(head != null && time!=null){
                                    if(head.getText().toString().equals(topic.title)){
                                        time.setText(String.valueOf(topic.lastmessage));
                                    }
                                }

                               /* if(head.getText().toString().equals(topic.title)){
                                    time.setText(String.valueOf(topic.lastmessage));
                                }

                                /*if (viewChild2 instanceof EditText) {

                                } else if (viewChild2 instanceof TextView && viewChild2.getTag() != null) {
                                    String text = ((TextView) viewChild2).getText().toString();
                                    if (text.equals(topic.title)) {
                                        Log.d(TAG, text);
                                        Log.d(TAG, "LEIDSIN PEALKIRJAS");
                                        ((TextView)viewChild2).setText(String.valueOf(topic.lastmessage));

                                    }

                            }
                            }

                            }
                        }


              /*  for(int i=0;i<tableLayout.getChildCount();i++)
                {
                    TableRow row = (TableRow)tableLayout.getChildAt(i);

                    TextView text = (TextView)row.getChildAt(2);// get child index on particular row
                    Log.d(TAG, text.getText().toString());
                        if(text.getText().equals(topic.title)){
                            text.setText(String.valueOf(topic.lastmessage));
                        }

                }


            /*    for(int i = 0; i < tableLayout.getChildCount(); i++){
                    TableRow row = (TableRow)tableLayout.getChildAt(i);
                    Text editText = (TextView)row.getChildAt(1);
                    if(editText.getValue().equals(topic.lastmessage)){
                        editText.setText
                        editText.v(String.valueOf(topic.lastmessage));
                    }
                }
                */
                    }



            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void searchTopics(final String query){
        if(tableLayout.getChildCount() > 1) {
            tableLayout.removeViews(1, tableLayout.getChildCount() -1);
        }
        database = FirebaseDatabase.getInstance();
        Query query1 =  database.getReference("Chats")
                .orderByChild("featured");

        query1.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Topic topic = dataSnapshot.getValue(Topic.class);

                if(topic.title.trim().toLowerCase().contains(query.trim().toLowerCase())) {
                    TableRow row = new TableRow(getApplicationContext());
                    TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(lp);
                    TextView textView = new TextView(getApplicationContext());
                    if(topic.featured == false) {
                        textView.setText(topic.title + "\n" + "Featured");
                    } else {
                        textView.setText(topic.title +  "\n" + "Not featured");
                    }
                    TextView timestamp1 = new TextView(getApplicationContext());
                    if (topic.lastmessage != 0) {
                        long oneDayInMillis = TimeUnit.DAYS.toMillis(1);

                        long timeDifference = System.currentTimeMillis() - topic.lastmessage;

                        if (timeDifference < oneDayInMillis) {
                            timestamp1.setText("Last activity " +"\n" +  DateFormat.format("hh:mm a", topic.lastmessage).toString());
                        } else {

                            timestamp1.setText("Last activity "+"\n" +  DateFormat.format("dd MMM - hh:mm a", topic.lastmessage).toString());
                        }
                    } else {
                        timestamp1.setText("No messages");
                    }
                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageView.setLayoutParams(new TableRow.LayoutParams(300, 300));
                    imageView.setPadding(5, 5, 5, 75);
                    textView.setPadding(70, 35, 35, 15);
                    textView.setTextSize(20);
                    new DownLoadImageTask(imageView).execute(topic.image);
                    row.setOnClickListener(onClickListener);
                    row.setTag(topic.title);
                    timestamp1.setPadding(40, 10, 10, 50);
                    row.addView(imageView);
                    row.addView(textView);
                    row.addView(timestamp1);
                    tableLayout.addView(row);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.searchmenu, menu);
        MenuItem item = menu.findItem(R.id.search_menu);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()) {
                   searchTopics(query);
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)){
                   startActivity(new Intent(getApplicationContext(), Topics.class));
                }
                return false;
            }
        });



        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }


}
