package com.rasmus.rachat;

import android.*;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;

import static com.rasmus.rachat.R.id.changeImage;
import static com.rasmus.rachat.R.id.chatName;

/**
 * Created by arvuti on 19/03/2017.
 */

public class AddChat extends AppCompatActivity {

    private TableLayout tableLayout;
    private TextView textView;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private StorageReference mStorageRef;
    private String TAG = "MAIN";
    private List<String> existingChats = new ArrayList<>();
    private Button addImage;
    private Button makeChat;
    private URI imageUri;
    private Button cameraPhoto;
    private EditText chatName;
    private Uri downloadedUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addchat);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);
        tableLayout = (TableLayout) findViewById(R.id.tableForChat);
        addImage = (Button) findViewById(R.id.imageAdd);
        makeChat = (Button) findViewById(R.id.makeChat);
        cameraPhoto = (Button) findViewById(R.id.cameraImage);
        chatName = (EditText) findViewById(R.id.chatName);
        populateTable();


        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    Log.d(TAG, "OLEN SISSE LOGITUD");
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        cameraPhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.CAMERA;
                config.isAllowMultipleSelect = false;
                EZPhotoPick.startPhotoPickActivity(AddChat.this, config);



            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.action_all_chats) {
                    Intent intent = new Intent(getApplicationContext(), Topics.class);
                    startActivity(intent);
                } else if(item.getItemId() == R.id.action_profile){
                    Intent intent = new Intent(getApplicationContext(), UserProfile.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), AddChat.class);
                    startActivity(intent);
                }
                return true;
            }


        });

        addImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.GALERY;
                config.isAllowMultipleSelect = false;
                EZPhotoPick.startPhotoPickActivity(AddChat.this, config);



            }
        });

        makeChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean contains = false;

                String chatsName = chatName.getText().toString().trim();
                for (String str : existingChats) {
                    if(str != null) {
                        if (str.trim().toLowerCase().equals(chatsName.toLowerCase()) || chatsName.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Topic exists", Toast.LENGTH_SHORT).show();
                            contains = true;
                        }
                    }
                }
                if (!contains) {
                    makeChat.setEnabled(false);
                    if (imageUri != null) {
                        uploadImageToStorage(imageUri, chatsName);
                        SystemClock.sleep(4000);


                    }
                    database = FirebaseDatabase.getInstance();
                    myRef = database.getReference("Chats");


                    if (downloadedUri != null) {
                        Topic topic = new Topic(chatsName, downloadedUri.toString(), true, 0);

                        myRef.push().setValue(topic);

                    } else {
                        Topic topic = new Topic(chatsName, "", true, 0);
                        myRef.push().setValue(topic);
                    }

                    if(downloadedUri != null && imageUri != null) {
                        downloadedUri = null;
                        imageUri = null;
                    }
                    cameraPhoto.setText("Take picture");
                    addImage.setText("Gallery picture");
                    makeChat.setEnabled(true);
                    chatName.setText("");
                }


            }

    });
    }


    public void uploadImageToStorage(URI uri, final String chatName) {
        final URI url = uri;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Uri file = Uri.fromFile(new File(url.toString()));
                StorageReference profileRef = mStorageRef.child("topicPictures/" + chatName);
                profileRef.putFile(file)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                downloadedUri = taskSnapshot.getDownloadUrl();


                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {

                            }
                        });

            }
        }).start();




    }

    public void populateTable() {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Chats");
      myRef.addChildEventListener(new ChildEventListener() {
          @Override
          public void onChildAdded(DataSnapshot dataSnapshot, String s) {
              Topic topic = dataSnapshot.getValue(Topic.class);
                  existingChats.add(topic.title);
                  TableRow row = new TableRow(getApplicationContext());
                  TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                  row.setLayoutParams(lp);
                  TextView textView = new TextView(getApplicationContext());
                  textView.setText(topic.title);
                  textView.setPadding(35, 35, 35, 35);
                  textView.setTextSize(20);
                  row.addView(textView);
                   row.setPadding(0,0,0,80);
                  tableLayout.addView(row);


          }

          @Override
          public void onChildChanged(DataSnapshot dataSnapshot, String s) {

          }

          @Override
          public void onChildRemoved(DataSnapshot dataSnapshot) {

          }

          @Override
          public void onChildMoved(DataSnapshot dataSnapshot, String s) {

          }

          @Override
          public void onCancelled(DatabaseError databaseError) {

          }
      });
    }


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == EZPhotoPick.PHOTO_PICK_GALERY_REQUEST_CODE || requestCode == EZPhotoPick.PHOTO_PICK_CAMERA_REQUEST_CODE) {
            try {
                Bitmap pickedPhoto = new EZPhotoPickStorage(this).loadLatestStoredPhotoBitmap();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                pickedPhoto.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(), pickedPhoto, "Title", null);
                String uri = getRealPathFromURI(Uri.parse(path));
                imageUri = URI.create(uri);
                Toast.makeText(getApplicationContext(), "Photo uploaded", Toast.LENGTH_SHORT).show();
                cameraPhoto.setText("Picture added");
                addImage.setText("Picture added");

            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Photo not uploaded", Toast.LENGTH_SHORT).show();
            }

        }

    }


    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


}

