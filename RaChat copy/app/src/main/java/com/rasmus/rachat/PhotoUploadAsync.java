package com.rasmus.rachat;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

/**
 * Created by arvuti on 21/03/2017.
 */

public class PhotoUploadAsync extends AsyncTask<String, Void, String> {

    FirebaseDatabase database;
    DatabaseReference myRef;
    private StorageReference mStorageRef;

    public String TAG = "MAIN";

    public String uri;

    public String chatName;

    public String returnURL;

    PhotoUploadAsync(String URI, String chatName){
        this.uri = URI;
    }


    @Override
    protected String doInBackground(String... params) {

        mStorageRef = FirebaseStorage.getInstance().getReference();
        Uri file = Uri.fromFile(new File(uri.toString()));
        StorageReference profileRef = mStorageRef.child("topicPictures/" + chatName);
        profileRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.d(TAG, "Tulin siiiia");
                        returnURL= taskSnapshot.getDownloadUrl().toString();





                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                    }
                });
        return returnURL;

    }

    @Override
    protected void onPostExecute(String s) {
        uri = returnURL;

    }
}
